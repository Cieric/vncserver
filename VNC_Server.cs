﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityVNC;

public class VNC_Server : MonoBehaviour
{
	[SerializeField]
	RenderTexture renderTexture;

	Texture2D tex;
	Rectangle fullScreenRect;
	Rect unityFullScreenRect;

	VNCServer server;

	private PixelFormat framebufferFromTextureFormat(TextureFormat format)
	{
		switch(format)
		{
			case TextureFormat.ARGB32: return new PixelFormat(32, 24, 1, 1, 255, 255, 255, 0, 8, 16);
			default:
				throw new System.NotImplementedException();
		}
	}

	private void Start()
	{
		tex = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
		server = new VNCServer();
		PixelFormat pixelFormat = framebufferFromTextureFormat(tex.format);
		server.frameBuffer = new FrameBuffer((ushort)renderTexture.width, (ushort)renderTexture.height, pixelFormat);
		server.Start();

		fullScreenRect = new Rectangle(0, 0, server.frameBuffer.Width, server.frameBuffer.Height);
		unityFullScreenRect = new Rect(0, 0, renderTexture.width, renderTexture.height);

		server.ClickEvent += Server_ClickEvent;
	}

	private void Server_ClickEvent(byte buttonMask, ushort x, ushort y)
	{
		Debug.Log("" + buttonMask + " " + x + " " + y);
	}

	void LateUpdate()
    {
		RenderTexture.active = renderTexture;
		tex.ReadPixels(unityFullScreenRect, 0, 0, false);
		tex.Apply(false);
		RenderTexture.active = null;
		byte[] data = tex.GetRawTextureData();
		server.WriteToScreen(fullScreenRect, data);
	}
}
