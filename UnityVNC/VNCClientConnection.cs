﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace UnityVNC
{
	class VNCClientConnection : Connection
	{
		public List<Rectangle> dirty;
		public bool IsDirty
		{
			get { return dirty.Count > 0; }
		}
		public Encodings[] encodings;
		public PixelFormat format;
		public VNCClientConnection(Connection connection) : base(connection) {
			dirty = new List<Rectangle>();
			format = new PixelFormat();
		}
	}
}
