﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace UnityVNC
{
	struct Converter
	{

		public static void Write(byte v, byte[] buffer, int offset)
		{
			buffer[offset++] = v;
		}

		public static void Write(short v, byte[] buffer, int offset)
		{
			buffer[offset++] = (byte)v;
			buffer[offset++] = (byte)(v >> 8);
		}

		public static void Write(ushort v, byte[] buffer, int offset)
		{
			buffer[offset++] = (byte)v;
			buffer[offset++] = (byte)(v >> 8);
		}

		public static void Write(int v, byte[] buffer, int offset)
		{
			buffer[offset++] = (byte)v;
			buffer[offset++] = (byte)(v >> 8);
			buffer[offset++] = (byte)(v >> 16);
			buffer[offset++] = (byte)(v >> 24);
		}

		public static void Write(uint v, byte[] buffer, int offset)
		{
			buffer[offset++] = (byte)v;
			buffer[offset++] = (byte)(v >> 8);
			buffer[offset++] = (byte)(v >> 16);
			buffer[offset++] = (byte)(v >> 24);
		}

		public static void Write(long v, byte[] buffer, int offset)
		{
			buffer[offset++] = (byte)v;
			buffer[offset++] = (byte)(v >> 8);
			buffer[offset++] = (byte)(v >> 16);
			buffer[offset++] = (byte)(v >> 24);
			buffer[offset++] = (byte)(v >> 32);
			buffer[offset++] = (byte)(v >> 40);
			buffer[offset++] = (byte)(v >> 48);
			buffer[offset++] = (byte)(v >> 56);
		}

		public static void Write(ulong v, byte[] buffer, int offset)
		{
			buffer[offset++] = (byte)v;
			buffer[offset++] = (byte)(v >> 8);
			buffer[offset++] = (byte)(v >> 16);
			buffer[offset++] = (byte)(v >> 24);
			buffer[offset++] = (byte)(v >> 32);
			buffer[offset++] = (byte)(v >> 40);
			buffer[offset++] = (byte)(v >> 48);
			buffer[offset++] = (byte)(v >> 56);
		}
	}
}
