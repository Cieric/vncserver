﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityVNC
{
	class ResizeableBuffer
	{
		byte[] buffer;
		int writePos = 0;

		public ResizeableBuffer()
		{
			buffer = new byte[512];
		}

		public void Write(byte b)
		{
			Resize(writePos + 1);
			buffer[writePos] = b;
			writePos += 1;
		}

		public void Write(byte[] bytes)
		{
			Resize(writePos + bytes.Length);
			Array.Copy(bytes, 0, buffer, writePos, bytes.Length);
			writePos += bytes.Length;
		}

		public void Write(byte[] bytes, int start, int length)
		{
			Resize(writePos + length);
			Array.Copy(bytes, start, buffer, writePos, length);
			writePos += length;
		}

		public void Resize(int size)
		{
			if (size > buffer.Length)
				Array.Resize(ref buffer, size);
		}

		public byte[] Raw()
		{
			return buffer;
		}

		public int Count()
		{
			return writePos;
		}
		
		public void Reset()
		{
			writePos = 0;
		}
	}
}
