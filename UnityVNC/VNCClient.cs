﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;

namespace UnityVNC
{
	public class VNCClient
	{
		public FrameBuffer frameBuffer;
		string ip;
		ushort port;
		string name;

		Encodings[] availableEncodings = { Encodings.Raw };

		bool running = false;
		public bool IsRunning
		{
			get { return running; }
		}

		public VNCClient(string ip, ushort port)
		{
			this.ip = ip;
			this.port = port;
		}

		public void Start()
		{
			Connection connection = new Connection(ip, port);
			var vncconnection = new VNCServerConnection(connection);
			Thread clientThread = new Thread(new ParameterizedThreadStart(onServerConnect));
			clientThread.Start(vncconnection);
		}

		private void onServerConnect(object data)
		{
			var vncconnection = (VNCServerConnection)data;
			Console.WriteLine("Server Connection Accepted!");
			SendProtocolVersion(vncconnection);
			SendSecurityVersion(vncconnection);
			SendShareFlag(vncconnection);
			RectServerConfig(vncconnection);
			SendFrameBufferFormat(vncconnection);
			SendEncodingFormats(vncconnection);
			StartUpdateLoop(vncconnection);
		}
		private void SendProtocolVersion(VNCServerConnection connection)
		{
			byte[] data = connection.Recv(12);
			connection.Send(Encoding.ASCII.GetBytes("RFB 003.008\n"));
		}

		private void SendSecurityVersion(VNCServerConnection connection)
		{
			byte securityVersionCount = connection.RecvByte();
			byte[] securityVersion = connection.Recv(securityVersionCount);
			connection.Send((byte)1);
			uint reasonLength = (uint)connection.RecvInt32();
			if(reasonLength > 0)
			{
				byte[] reason = connection.Recv((int)reasonLength);
				Console.WriteLine(Encoding.ASCII.GetString(reason));
			}
		}
		private void SendShareFlag(VNCServerConnection connection)
		{
			connection.Send((byte)1);
		}

		private void SendFrameBufferFormat(VNCServerConnection connection)
		{
			connection.Buffer((byte)ClientMessageType.SetPixelFormat);
			connection.Buffer(new byte[3] { 0, 0, 0 });
			connection.Buffer(frameBuffer.PixelFormat.bitsPerPixel);
			connection.Buffer(frameBuffer.PixelFormat.depth);
			connection.Buffer(frameBuffer.PixelFormat.bigEndianFlag);
			connection.Buffer(frameBuffer.PixelFormat.trueColourFlag);
			connection.Buffer(frameBuffer.PixelFormat.redMax);
			connection.Buffer(frameBuffer.PixelFormat.greenMax);
			connection.Buffer(frameBuffer.PixelFormat.blueMax);
			connection.Buffer(frameBuffer.PixelFormat.redShift);
			connection.Buffer(frameBuffer.PixelFormat.greenShift);
			connection.Buffer(frameBuffer.PixelFormat.blueShift);
			connection.Buffer(new byte[3] { 0, 0, 0 });
			connection.Flush();
		}

		private void SendEncodingFormats(VNCServerConnection connection)
		{
			connection.Buffer((byte)ClientMessageType.SetEncodings);
			connection.Buffer(new byte[1] { 0 });
			connection.Buffer((ushort)availableEncodings.Length);
			foreach (Encodings encoding in availableEncodings)
				connection.Buffer((int)encoding);
			connection.Flush();
		}

		private void RectServerConfig(VNCServerConnection connection)
		{
			frameBuffer = new FrameBuffer(
				connection.RecvUInt16(),
				connection.RecvUInt16()
			);
			frameBuffer.PixelFormat.bitsPerPixel = connection.RecvByte();
			frameBuffer.PixelFormat.depth = connection.RecvByte();
			frameBuffer.PixelFormat.bigEndianFlag = connection.RecvByte();
			frameBuffer.PixelFormat.trueColourFlag = connection.RecvByte();
			frameBuffer.PixelFormat.redMax = connection.RecvUInt16();
			frameBuffer.PixelFormat.greenMax = connection.RecvUInt16();
			frameBuffer.PixelFormat.blueMax = connection.RecvUInt16();
			frameBuffer.PixelFormat.redShift = connection.RecvByte();
			frameBuffer.PixelFormat.greenShift = connection.RecvByte();
			frameBuffer.PixelFormat.blueShift = connection.RecvByte();
			connection.Recv(3);
			uint nameLength = connection.RecvUInt32();
			name = Encoding.ASCII.GetString(connection.Recv((int)nameLength));
		}

		private void StartUpdateLoop(VNCServerConnection connection)
		{
			running = true;
			while (connection.IsConnected)
			{
				if(frameBuffer.IsDirty)
				{
					foreach(Rectangle rect in frameBuffer.dirty)
					{
						connection.Buffer((byte)ClientMessageType.FramebufferUpdateRequest);
						connection.Buffer((byte)0); //Incremental
						connection.Buffer((ushort)rect.x);
						connection.Buffer((ushort)rect.y);
						connection.Buffer((ushort)rect.width);
						connection.Buffer((ushort)rect.height);
						connection.Flush();
					}
					frameBuffer.dirty.Clear();
				}

				if (connection.Available())
				{
					ServerMessageType command = (ServerMessageType)connection.RecvByte();
					switch (command)
					{
						case ServerMessageType.FramebufferUpdate: {
							Console.WriteLine("updating");
							connection.RecvByte();
							ushort rectCount = connection.RecvUInt16();
							for(int i=0; i<rectCount; ++i)
							{
								ushort xPos = connection.RecvUInt16();
								ushort yPos = connection.RecvUInt16();
								ushort width = connection.RecvUInt16();
								ushort height = connection.RecvUInt16();
								uint encoding = connection.RecvUInt32();
								Rectangle rect = new Rectangle(xPos, yPos, width, height);
								int bufferSize = rect.Area * frameBuffer.PixelFormat.bitsPerPixel / 8;
								byte[] data = new byte[bufferSize];
								connection.Recv(data, bufferSize);
								frameBuffer.WriteSection(data, new Rectangle(xPos, yPos, width, height));
							}
							Console.WriteLine("updated");
						} break;
						default:
							throw new NotImplementedException($"Command {command} not yet implemented!");
					}
				}
			}
		}

		public void FrameBufferUpdateRequest(Rectangle rect)
		{
			frameBuffer.dirty.Add(rect);
		}
	}
}
