﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.IO;

namespace UnityVNC
{
	class ConnectionListener
	{
		TcpListener listener;

		public delegate void ClientConnectHandler(Connection connection);
		public event ClientConnectHandler clientConnectEvent;

		public ConnectionListener(string address, ushort port)
		{
			IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(address), port);
			listener = new TcpListener(endPoint);
		}

		public void AcceptConnections()
		{
			listener.Start();
			listener.BeginAcceptTcpClient(new AsyncCallback(DoAcceptSocketCallback), this);
		}

		public static void DoAcceptSocketCallback(IAsyncResult ar)
		{
			ConnectionListener connectionListener = (ConnectionListener)ar.AsyncState;
			TcpClient client = connectionListener.listener.EndAcceptTcpClient(ar);
			client.SendTimeout = 1000;
			client.ReceiveTimeout = 1000;
			Connection connection = new Connection(client);
			connectionListener.clientConnectEvent.Invoke(connection);
			connectionListener.AcceptConnections();
		}
	}

	class Network
	{
		static public UInt16 HostToNetwork(UInt16 value) { return (UInt16)IPAddress.NetworkToHostOrder((Int16)value); }
		static public UInt16 NetworkToHost(UInt16 value) { return (UInt16)IPAddress.NetworkToHostOrder((Int16)value); }
		static public Int16 HostToNetwork(Int16 value) { return IPAddress.NetworkToHostOrder(value); }
		static public Int16 NetworkToHost(Int16 value) { return IPAddress.NetworkToHostOrder(value); }
		static public UInt32 HostToNetwork(UInt32 value) { return (UInt32)IPAddress.NetworkToHostOrder((Int32)value); }
		static public UInt32 NetworkToHost(UInt32 value) { return (UInt32)IPAddress.NetworkToHostOrder((Int32)value); }
		static public Int32 HostToNetwork(Int32 value) { return IPAddress.NetworkToHostOrder(value); }
		static public Int32 NetworkToHost(Int32 value) { return IPAddress.NetworkToHostOrder(value); }
	}

	class Connection
	{
		TcpClient tcpClient;
		byte[] buffer;
		ResizeableBuffer writeBuffer;
		ResizeableBuffer readBuffer;
		public bool IsConnected
		{
			get { return tcpClient.Connected; }
		}

		NetworkStream Stream
		{
			get { return tcpClient.GetStream(); }
		}

		public Connection(string address, ushort port)
		{
			tcpClient = new TcpClient();
			tcpClient.Connect(new IPEndPoint(IPAddress.Parse(address), port));
			buffer = new byte[512];
			writeBuffer = new ResizeableBuffer();
			readBuffer = new ResizeableBuffer();
		}

		public Connection(TcpClient tcpClient)
		{
			this.tcpClient = tcpClient;
			buffer = new byte[512];
			writeBuffer = new ResizeableBuffer();
			readBuffer = new ResizeableBuffer();
		}

		public Connection(Connection connection)
		{
			this.tcpClient = connection.tcpClient;
			buffer = new byte[512];
			writeBuffer = new ResizeableBuffer();
			readBuffer = new ResizeableBuffer();
		}

		public void Disconnect()
		{
			tcpClient.Close();
		}

		public void Send(byte[] b)
		{
			Stream.Write(b, 0, b.Length);
		}

		public void Send(byte b)
		{
			Buffer(b); Flush();
		}

		public void Send(UInt16 b)
		{
			Buffer(b); Flush();
		}

		public void Send(UInt32 b)
		{
			Buffer(b); Flush();
		}

		public bool Available()
		{
			return tcpClient.Available > 0;
		}

		public byte[] Recv(int count)
		{
			if(count > 512)
			{
				throw new IndexOutOfRangeException();
			}
			Stream.Read(buffer, 0, count);
			return buffer;
		}

		public void Recv(byte[] buffer, int count)
		{
			int offset = 0;
			while (count > 0)
			{
				int readAmount = tcpClient.Available;
				Stream.Read(buffer, offset, Math.Min(count, readAmount));
				count -= readAmount;
				offset += readAmount;
			}
		}

		public byte RecvByte()
		{
			Stream.Read(buffer, 0, 1);
			return buffer[0];
		}

		public ushort RecvUInt16()
		{
			Stream.Read(buffer, 0, 2);
			return Network.NetworkToHost(BitConverter.ToUInt16(buffer, 0));
		}

		public uint RecvUInt32()
		{
			Stream.Read(buffer, 0, 4);
			return Network.NetworkToHost(BitConverter.ToUInt32(buffer, 0));
		}

		public int RecvInt32()
		{
			Stream.Read(buffer, 0, 4);
			return Network.NetworkToHost(BitConverter.ToInt32(buffer, 0));
		}

		public void Buffer(byte[] b)
		{
			writeBuffer.Write(b);
		}

		public void Buffer(byte[] b, int start, int length)
		{
			writeBuffer.Write(b, start, length);
		}

		public void Buffer(byte b)
		{
			writeBuffer.Write(b);
		}

		byte[] tempBuffer = new byte[4];
		public void Buffer(Int16 b)
		{
			Converter.Write(Network.HostToNetwork(b), tempBuffer, 0);
			writeBuffer.Write(tempBuffer, 0, 2);
		}

		public void Buffer(UInt16 b)
		{
			Converter.Write(Network.HostToNetwork(b), tempBuffer, 0);
			writeBuffer.Write(tempBuffer, 0, 2);
		}

		public void Buffer(Int32 b)
		{
			Converter.Write(Network.HostToNetwork(b), tempBuffer, 0);
			writeBuffer.Write(tempBuffer, 0, 4);
		}
		public void Buffer(UInt32 b)
		{
			Converter.Write(Network.HostToNetwork(b), tempBuffer, 0);
			writeBuffer.Write(tempBuffer, 0, 4);
		}

		public void Flush()
		{
			Stream.Write(writeBuffer.Raw(), 0, writeBuffer.Count());
			writeBuffer.Reset();
		}
	}
}
