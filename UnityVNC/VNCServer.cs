﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace UnityVNC
{
   public class VNCServer
   {
		public FrameBuffer frameBuffer;
		ConnectionListener listener;
		ResizeableBuffer tempBuffer;
		byte[] convertFormatBuffer;
		byte[] convertFormatTempBuffer;
		bool running = false;

		public delegate void ClickEventHandler(byte buttonMask, UInt16 x, UInt16 y);
		public event ClickEventHandler ClickEvent;

		bool shareDesktop = true;

		Encodings[] availableEncodings = { Encodings.Raw };

		public bool IsRunning
		{
			get { return running; }
		}

		public VNCServer()
		{
			listener = new ConnectionListener("127.0.0.1", 5900);
			tempBuffer = new ResizeableBuffer();
			convertFormatBuffer = new byte[512];
			convertFormatTempBuffer = new byte[4];
		}

		public void Start()
		{
			listener.clientConnectEvent += (Connection connection) => {
				var vncconnection = new VNCClientConnection(connection);
				Thread clientThread = new Thread(new ParameterizedThreadStart(onClientConnect));
				clientThread.Start(vncconnection);
			};
			listener.AcceptConnections();
			running = true;
		}

		private void onClientConnect(object data)
		{
			var vncconnection = (VNCClientConnection)data;
			Console.WriteLine("Client Connection Accepted!");
			SendProtocolVersion(vncconnection);
			SendSecurityVersion(vncconnection);
			RecvShareFlag(vncconnection);
			SendServerConfig(vncconnection);
			StartUpdateLoop(vncconnection);
		}

		private void SendProtocolVersion(VNCClientConnection connection)
		{
			connection.Send(Encoding.ASCII.GetBytes("RFB 003.008\n"));
			connection.Recv(12);
		}

		private void SendSecurityVersion(VNCClientConnection connection)
		{
			connection.Send(new byte[] { 1, 1 });
			byte securityType = connection.RecvByte();
			connection.Send(new byte[] { 0, 0, 0, 0 });
		}

		private void RecvShareFlag(VNCClientConnection connection)
		{
			shareDesktop = (connection.RecvByte()!=0);
		}

		private void SendServerConfig(VNCClientConnection connection)
		{
			connection.Buffer(frameBuffer.Width);
			connection.Buffer(frameBuffer.Height);
			connection.Buffer(frameBuffer.PixelFormat.bitsPerPixel);
			connection.Buffer(frameBuffer.PixelFormat.depth);
			connection.Buffer(frameBuffer.PixelFormat.bigEndianFlag);
			connection.Buffer(frameBuffer.PixelFormat.trueColourFlag);
			connection.Buffer(frameBuffer.PixelFormat.redMax);
			connection.Buffer(frameBuffer.PixelFormat.greenMax);
			connection.Buffer(frameBuffer.PixelFormat.blueMax);
			connection.Buffer(frameBuffer.PixelFormat.redShift);
			connection.Buffer(frameBuffer.PixelFormat.greenShift);
			connection.Buffer(frameBuffer.PixelFormat.blueShift);
			connection.Buffer(new byte[] { 0, 0, 0 });
			byte[] name = Encoding.UTF8.GetBytes("UnityVNC");
			connection.Buffer(name.Length);
			connection.Buffer(name);
			connection.Flush();
		}

		private Tuple<byte[],int> ConvertFormat(byte[] srcBuffer, int srcStart, int srcCount, PixelFormat srcFormat, PixelFormat dstFormat)
		{
			int size;
			if (srcFormat.bitsPerPixel == dstFormat.bitsPerPixel)
			{
				size = srcCount;
				if (convertFormatBuffer.Length < size)
					Array.Resize(ref convertFormatBuffer, size);
				var BytesPP = srcFormat.bitsPerPixel / 8;
				for (int i = 0; i < srcCount; i+=BytesPP)
				{
					UInt32 color = BitConverter.ToUInt32(srcBuffer, srcStart+i);
					UInt32 red = (((color >> srcFormat.redShift) & srcFormat.redMax) * dstFormat.redMax / srcFormat.redMax) << dstFormat.redShift;
					UInt32 green = (((color >> srcFormat.greenShift) & srcFormat.greenMax) * dstFormat.greenMax / srcFormat.greenMax) << dstFormat.greenShift;
					UInt32 blue = (((color >> srcFormat.blueShift) & srcFormat.blueMax) * dstFormat.blueMax / srcFormat.blueMax) << dstFormat.blueShift;
					UInt32 nColor = red | green | blue;
					Converter.Write(nColor, convertFormatTempBuffer, 0);
					Buffer.BlockCopy(convertFormatTempBuffer, 0, convertFormatBuffer, i, BytesPP);
				}
			}
			else
			{
				throw new NotImplementedException();
			}
			
			return Tuple.New(convertFormatBuffer, size);
		}

		private void SendFrameBufferUpdate(ref VNCClientConnection connection)
		{
			var bpp = frameBuffer.PixelFormat.BytesPerPixel;

			connection.Buffer((byte)ServerMessageType.FramebufferUpdate);
			connection.Buffer((byte)0); // Padding
			connection.Buffer((ushort)connection.dirty.Count);
			foreach (Rectangle rect in connection.dirty)
			{
				connection.Buffer((ushort)rect.x);
				connection.Buffer((ushort)rect.y);
				connection.Buffer((ushort)rect.width);
				connection.Buffer((ushort)rect.height);
				///TODO: change to be dynamic
				connection.Buffer((int)0); //Encoding
				tempBuffer.Resize(rect.Area * bpp);
				frameBuffer.GetSection(tempBuffer.Raw(), rect);
				var buffer = ConvertFormat(tempBuffer.Raw(), 0, rect.Area * bpp, frameBuffer.PixelFormat, connection.format);
				connection.Buffer(buffer.First, 0, buffer.Second);
			}
			connection.Flush();
			connection.dirty.Clear();
		}

		private void StartUpdateLoop(VNCClientConnection connection)
		{
			while(connection.IsConnected)
			{
				if (connection.IsDirty)
					SendFrameBufferUpdate(ref connection);

				if (connection.Available())
				{
					ClientMessageType command = (ClientMessageType)connection.RecvByte();
					switch (command)
					{
						case ClientMessageType.SetPixelFormat:
							connection.Recv(3); //Padding
							connection.format.bitsPerPixel = connection.RecvByte();
							connection.format.depth = connection.RecvByte();
							connection.format.bigEndianFlag = connection.RecvByte();
							connection.format.trueColourFlag = connection.RecvByte();
							connection.format.redMax = connection.RecvUInt16();
							connection.format.greenMax = connection.RecvUInt16();
							connection.format.blueMax = connection.RecvUInt16();
							connection.format.redShift = connection.RecvByte();
							connection.format.greenShift = connection.RecvByte();
							connection.format.blueShift = connection.RecvByte();
							connection.Recv(3); //Padding
							break;
						case ClientMessageType.SetEncodings:
							connection.Recv(1); //Padding
							uint numberOfEncodings = connection.RecvUInt16();
							Encodings[] encodings = new Encodings[numberOfEncodings];
							for (int i = 0; i < numberOfEncodings; ++i)
								encodings[i] = (Encodings)connection.RecvInt32();
							connection.encodings = encodings.Intersect(availableEncodings).ToArray();
							break;
						case ClientMessageType.FramebufferUpdateRequest:
							var incremental = connection.RecvByte();
							//byte[] rect = connection.Recv(8);
							var x = connection.RecvUInt16();
							var y = connection.RecvUInt16();
							var width = connection.RecvUInt16();
							var height = connection.RecvUInt16();
							Rectangle rectangle = new Rectangle(x,y,width,height);
							connection.dirty.Add(rectangle);
							break;
						case ClientMessageType.KeyEvent:
							connection.Recv(7); //Drop Message
							break;
						case ClientMessageType.PointerEvent:
							connection.Recv(5); //Drop Message
							byte buttonMask = connection.RecvByte();
							UInt16 xPosition = connection.RecvUInt16();
							UInt16 yPosition = connection.RecvUInt16();
							ClickEvent?.Invoke(buttonMask, xPosition, yPosition);
							break;
						case ClientMessageType.ClientCutText:
							connection.Recv(3); //Drop Message
							connection.Recv((int)connection.RecvUInt32());
							break;
						default:
							throw new NotImplementedException($"Command {command} not yet implemented!");
					}
				}
			}
		}

		public void WriteToScreen(Rectangle rect, byte[] data)
		{
			frameBuffer.WriteSection(data, rect);
		}
	}
}
