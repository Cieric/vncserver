﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityVNC
{
	public class PixelFormat
	{
		public byte bitsPerPixel;
		public byte depth;
		public byte bigEndianFlag;
		public byte trueColourFlag;
		public ushort redMax;
		public ushort greenMax;
		public ushort blueMax;
		public byte redShift;
		public byte greenShift;
		public byte blueShift;
		short padding1;
		byte padding2;

		public int BytesPerPixel
		{
			get { return bitsPerPixel / 8; }
		}

		public PixelFormat()
		{
			bitsPerPixel = 32;
			depth = 24;
			bigEndianFlag = 0;
			trueColourFlag = 1;
			redMax = 255;
			greenMax = 255;
			blueMax = 255;
			redShift = 16;
			greenShift = 8;
			blueShift = 0;
		}

		public PixelFormat(
				byte bitsPerPixel,
				byte depth,
				byte bigEndianFlag,
				byte trueColourFlag,
				ushort redMax,
				ushort greenMax,
				ushort blueMax,
				byte redShift,
				byte greenShift,
				byte blueShift
			)
		{
			this.bitsPerPixel = bitsPerPixel;
			this.depth = depth;
			this.bigEndianFlag = bigEndianFlag;
			this.trueColourFlag = trueColourFlag;
			this.redMax = redMax;
			this.greenMax = greenMax;
			this.blueMax = blueMax;
			this.redShift = redShift;
			this.greenShift = greenShift;
			this.blueShift = blueShift;
		}
	}
}
