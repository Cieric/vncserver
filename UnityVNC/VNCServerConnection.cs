﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace UnityVNC
{
	class VNCServerConnection : Connection
	{
		public Encodings[] encodings;
		public PixelFormat format;
		public VNCServerConnection(Connection connection) : base(connection) {

			format = new PixelFormat();
		}
	}
}
