﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityVNC
{
	public class FrameBuffer
	{
		PixelFormat pixelFormat;
		UInt16 width;
		UInt16 height;
		public byte[] Screen;
		public List<Rectangle> dirty;

		public bool IsDirty
		{
			get { return dirty.Count > 0; }
		}

		public UInt16 Width
		{
			get { return width; }
		}

		public UInt16 Height
		{
			get { return height; }
		}

		public PixelFormat PixelFormat
		{
			get { return pixelFormat; }
		}

		public FrameBuffer(UInt16 width, UInt16 height)
		{
			this.dirty = new List<Rectangle>();
			this.pixelFormat = new PixelFormat();
			this.width = width;
			this.height = height;
			Screen = new byte[width * pixelFormat.BytesPerPixel * height];
		}

		public FrameBuffer(UInt16 width, UInt16 height, PixelFormat format)
		{
			this.dirty = new List<Rectangle>();
			this.pixelFormat = format;
			this.width = width;
			this.height = height;
			Screen = new byte[width * pixelFormat.BytesPerPixel * height];
		}

		public void Resize(UInt16 width, UInt16 height, bool clearScreen = true)
		{
			if(clearScreen)
			{
				Screen = new byte[width * pixelFormat.BytesPerPixel * height];
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public void GetSection(byte[] buffer, Rectangle rect)
		{
			if (rect.x == 0 && rect.y == 0 && rect.width == width && rect.height == height)
			{
				Buffer.BlockCopy(Screen, 0, buffer, 0, buffer.Length);
				return;
			}

			int start = (rect.y * width + rect.x) * pixelFormat.BytesPerPixel;
			int screenStride = width * pixelFormat.BytesPerPixel;
			int rectStride = rect.width * pixelFormat.BytesPerPixel;
			for (int y = 0; y < rect.height; ++y)
			{
				int offset = start + screenStride * y;
				Buffer.BlockCopy(Screen, offset, buffer, rectStride * y, rectStride);
			}
		}

		public void WriteSection(byte[] buffer, Rectangle rect)
		{
			if(rect.x == 0 && rect.y == 0 && rect.width == width && rect.height == height)
			{
				Buffer.BlockCopy(buffer, 0, Screen, 0, buffer.Length);
				return;
			}

			int start = (rect.y * width + rect.x) * pixelFormat.BytesPerPixel;
			int screenStride = width * pixelFormat.BytesPerPixel;
			int rectStride = rect.width * pixelFormat.BytesPerPixel;
			for (int y = 0; y < rect.height; ++y)
			{
				int offset = start + screenStride * y;
				Buffer.BlockCopy(buffer, rectStride * y, Screen, offset, rectStride);
			}
		}
	}
}
