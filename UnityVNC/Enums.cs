﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityVNC
{
	public enum ClientMessageType : byte
	{
		SetPixelFormat = 0,
		SetEncodings = 2,
		FramebufferUpdateRequest = 3,
		KeyEvent = 4,
		PointerEvent = 5,
		ClientCutText = 6,
	}

	public enum ServerMessageType : byte
	{
		FramebufferUpdate = 0,
	}

	public enum Encodings
	{
		Raw = 0,
 		CopyRect = 1,
 		RRE = 2,
 		CoRRE = 4,
 		Hextile = 5,
 		zlib = 6,
 		Tight = 7,
 		zlibhex = 8,
 		ZRLE = 16,
 		TightPNG = -260,
	}
}
