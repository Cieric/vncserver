﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityVNC
{
	public class Rectangle
	{
		public int x, y, width, height;

		public int Area
		{
			get { return width * height; }
		}

		public Rectangle(int x, int y, int width, int height)
		{
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}
	}
}
