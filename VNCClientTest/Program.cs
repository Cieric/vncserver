﻿using System;
using UnityVNC;

namespace VNCClientTest
{
	class Program
	{
		static void Main(string[] args)
		{
			VNCClient client = new VNCClient("127.0.0.1", 5900);
			client.Start();
			Rectangle rect = new Rectangle(0, 0, 1920, 1080);
			//server.WriteToScreen(rect, screen);
			while (!client.IsRunning) ;
			while (client.IsRunning)
			{
				if (Console.KeyAvailable)
				{
					var key = Console.ReadKey();
					switch(key.Key)
					{
						case ConsoleKey.P: {
							byte[] bytes = new byte[rect.Area * client.frameBuffer.PixelFormat.BytesPerPixel];
							client.frameBuffer.GetSection(bytes, rect);
							PPM.WriteBitmapToPPM("output.ppm", bytes, rect.width);
						} break;
						case ConsoleKey.D7: {
							client.FrameBufferUpdateRequest(rect);
						} break;
					}
				}
			}
		}
	}
}
