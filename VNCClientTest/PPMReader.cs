﻿using System.IO;

namespace VNCClientTest
{
	public class PPM
	{
		public static byte[] ReadImageFromPPM(string file)
		{
			var reader = new BinaryReader(new FileStream(file, FileMode.Open));
			if (reader.ReadChar() != 'P' || reader.ReadChar() != '6')
				return null;
			reader.ReadChar(); //Eat newline
			string widths = "", heights = "";
			char temp;
			while ((temp = reader.ReadChar()) != ' ')
				widths += temp;
			while ((temp = reader.ReadChar()) >= '0' && temp <= '9')
				heights += temp;
			if (reader.ReadChar() != '2' || reader.ReadChar() != '5' || reader.ReadChar() != '5')
				return null;
			reader.ReadChar(); //Eat the last newline
			int width = int.Parse(widths),
				height = int.Parse(heights);
			byte[] bitmap = new byte[width*height*4];
			//Read in the pixels
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					int offset = (x + y * width) * 4;
					bitmap[offset + 3] = 0;
					bitmap[offset + 2] = reader.ReadByte();
					bitmap[offset + 1] = reader.ReadByte();
					bitmap[offset + 0] = reader.ReadByte();
				}
			}
			return bitmap;
		}

		public static void WriteBitmapToPPM(string file, byte[] bitmap, int width)
		{
			int height = bitmap.Length / width / 4;
			//Use a streamwriter to write the text part of the encoding
			var writer = new StreamWriter(file);
			writer.Write("P6\n");
			writer.Write($"{width} {height}\n");
			writer.Write("255\n");
			writer.Close();
			//Switch to a binary writer to write the data
			var writerB = new BinaryWriter(new FileStream(file, FileMode.Append));
			for (int y = 0; y < height; y++)
				for (int x = 0; x < width; x++)
				{
					int offset = (x + y * width) * 4;
					writerB.Write(bitmap[offset + 2]);
					writerB.Write(bitmap[offset + 1]);
					writerB.Write(bitmap[offset + 0]);
				}
			writerB.Close();
		}
	}
}
