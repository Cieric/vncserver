﻿using System;
using System.Threading;
using UnityVNC;

namespace VNCServerTest
{
	class Program
	{
		static void Main(string[] args)
		{
			VNCServer server = new VNCServer();
			server.frameBuffer = new FrameBuffer(1920, 1080);
			server.Start();
			Rectangle rect = new Rectangle(0, 0, 1920, 1080);
			byte[] screen = PPM.ReadImageFromPPM("desktop.ppm");
			server.WriteToScreen(rect, screen);
			while (server.IsRunning)
			{
				if(Console.KeyAvailable)
				{
					var key = Console.ReadKey();
					if (key.Key == ConsoleKey.P)
					{
						byte[] bytes = new byte[rect.Area * server.frameBuffer.PixelFormat.BytesPerPixel];
						server.frameBuffer.GetSection(bytes, rect);
						PPM.WriteBitmapToPPM("output.ppm", bytes, rect.width);
					}
				}
			}
		}
	}
}
